# Fake or not backend

## Install

``` bash
git clone https://framagit.org/alcalyn/fake-or-not-backend.git
cd fake-or-not-backend/

# Install dependencies
composer install

# Import basic fixtures
bin/console app:articles:import ./articles.json
```

## Develop

``` bash
# Start development server
bin/console server:start
```
