.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-16s\033[0m %s\n", $$1, $$2}'

.PHONY: start
start: up ## Start development environemnt
	docker-compose exec -d php-fpm php bin/console server:run

.PHONY: init
init: up install db_create import_articles start ## Init development environment (after git clone)

.PHONY: update
update: install db_update ## Update environment

.PHONY: php
php: ## Enter in php container
	docker-compose exec php-fpm bash

.PHONY: up
up: ## Start docker containers
	docker-compose up -d

.PHONY: db_create
db_create: ## Create database schema
	docker-compose exec php-fpm php bin/console doctrine:schema:create

.PHONY: db_update
db_update: ## Update database schema
	docker-compose exec php-fpm php bin/console doctrine:schema:update --force

.PHONY: import_articles
import_articles: ## Import base articles in db
	docker-compose exec php-fpm php bin/console app:articles:import ./articles.json

.PHONY: install
install: ## Install dependencies
	docker-compose run composer install
