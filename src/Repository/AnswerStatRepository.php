<?php

namespace App\Repository;

use App\Entity\AnswerStat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AnswerStat|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnswerStat|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnswerStat[]    findAll()
 * @method AnswerStat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnswerStatRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AnswerStat::class);
    }
}
