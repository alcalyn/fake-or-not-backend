<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
     * @return Article[]
     */
    public function findEnabledArticles(): array
    {
        return $this->getEnabledArticlesQb()
            ->getQuery()
            ->getResult()
        ;
    }

    public function getEnabledArticlesQb(): QueryBuilder
    {
        return $this->createQueryBuilder('article')
            ->where('article.enabled = true')
        ;
    }

    /**
     * Return count of real and fake articles.
     *
     * @return int[]
     */
    public function countRealAndFakeArticles(): array
    {
        $articlesQueryBuilder = $this->getEnabledArticlesQb()
            ->select('count(article.title)')
        ;

        $real = (clone $articlesQueryBuilder)
            ->andWhere('article.fake = false')
            ->getQuery()
            ->getSingleScalarResult()
        ;

        $fake = (clone $articlesQueryBuilder)
            ->andWhere('article.fake = true')
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return [
            'real' => (int) $real,
            'fake' => (int) $fake,
        ];
    }

    /**
     * Find n random real articles.
     *
     * @param int $number Number of random real articles to find.
     * @param int[] $exceptIds
     *
     * @return Article[]
     */
    public function findRandomRealArticles(int $number, ?array $exceptIds = []): array
    {
        $articlesQueryBuilder = $this->createQueryBuilder('article')
            ->where('article.fake = false')
        ;

        return $this->findRandomArticlesFromQueryBuilder($articlesQueryBuilder, $number, $exceptIds);
    }

    /**
     * Find n random fake articles.
     *
     * @param int $number Number of random fake articles to find.
     * @param int[] $exceptIds
     *
     * @return Article[]
     */
    public function findRandomFakeArticles(int $number, ?array $exceptIds = []): array
    {
        $articlesQueryBuilder = $this->createQueryBuilder('article')
            ->where('article.fake = true')
        ;

        return $this->findRandomArticlesFromQueryBuilder($articlesQueryBuilder, $number, $exceptIds);
    }

    /**
     * Find n random articles from an article query builder.
     *
     * @param QueryBuilder $queryBuilder
     * @param int $number
     * @param int[] $exceptIds
     *
     * @return Article[]
     */
    private function findRandomArticlesFromQueryBuilder(QueryBuilder $queryBuilder, int $number, ?array $exceptIds = []): array
    {
        if (0 === $number) {
            return [];
        }

        $articleIdsQueryBuilder = $queryBuilder
            ->select('article.id')
            ->andWhere('article.enabled = true')
        ;

        if (count($exceptIds) > 0) {
            $articleIdsQueryBuilder
                ->andWhere('article.id not in (:exceptIds)')
                ->setParameter(':exceptIds', $exceptIds)
            ;
        }

        $articleIds = $articleIdsQueryBuilder
            ->getQuery()
            ->getResult()
        ;

        $articleIds = array_map(function (array $article) {
            return $article['id'];
        }, $articleIds);

        $randomIdKeys = array_rand($articleIds, $number);

        if (1 === $number) {
            return [
                $this->find($articleIds[$randomIdKeys]),
            ];
        }

        $randomIds = array_map(function (int $randomIdKey) use ($articleIds) {
            return $articleIds[$randomIdKey];
        }, $randomIdKeys);

        return $this->createQueryBuilder('article')
            ->where('article.id in (:randomIds)')
            ->setParameter(':randomIds', $randomIds)
            ->setMaxResults($number)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param int[] $ids
     *
     * @return Article[]
     */
    public function findArticlesByIds(array $ids): array
    {
        return $this->createQueryBuilder('article')
            ->where('article.id in (:ids)')
            ->setParameter(':ids', $ids)
            ->getQuery()
            ->getResult()
        ;
    }
}
