<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\Stats;
use JMS\Serializer\SerializerInterface;
use JMS\Serializer\SerializationContext;

class StatsProcessCommand extends Command
{
    protected static $defaultName = 'app:stats:process';

    /**
     * @var Stats
     */
    private $stats;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(Stats $stats, SerializerInterface $serializer)
    {
        parent::__construct();

        $this->stats = $stats;
        $this->serializer = $serializer;
    }

    protected function configure()
    {
        $this
            ->setDescription('Process stats from answered articles.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $statsContext = SerializationContext::create()
            ->setGroups(['Default', 'Stats'])
        ;

        $json = $this->serializer->serialize($this->stats->process(), 'json', $statsContext);

        $output->writeln($json);
    }
}
