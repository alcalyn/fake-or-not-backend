<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\ArticlesImporter;

class ArticlesImportCommand extends Command
{
    protected static $defaultName = 'app:articles:import';

    /**
     * @var ArticlesImporter
     */
    private $articlesImporter;

    public function __construct(ArticlesImporter $articlesImporter)
    {
        parent::__construct();

        $this->articlesImporter = $articlesImporter;
    }

    protected function configure()
    {
        $this
            ->setDescription('Import articles from json export.')
            ->addArgument('articles_json_file', InputArgument::REQUIRED, 'Path to articles.json export file.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $results = $this->articlesImporter->import($input->getArgument('articles_json_file'));

        $io->success(sprintf(
            'Import finished. %d articles added, %d articles not added because validation failed.',
            $results['imported'],
            $results['failed']
        ));

        if ($results['failed'] > 0) {
            $output->writeln($results['errors']);
        }
    }
}
