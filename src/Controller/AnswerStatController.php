<?php

namespace App\Controller;

use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use App\Entity\AnswerStat;
use App\Service\Stats;

class AnswerStatController extends FOSRestController
{
    /**
     * @Rest\Post("")
     *
     * @ParamConverter("answerStat", converter="fos_rest.request_body")
     *
     * @Rest\View
     *
     * @return View|void
     */
    public function postAnswerStatAction(AnswerStat $answerStat, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            return View::create($validationErrors, Response::HTTP_CONFLICT);
        }

        $answerStat->setCreatedAt(new \DateTime('now'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($answerStat);
        $em->flush();
    }

    /**
     * @Rest\Get("")
     * @Rest\View(serializerGroups={"Default", "Stats"})
     *
     * @Cache(expires="tomorrow", public=true)
     *
     * @return Article[][]
     */
    public function getStatsAction(Stats $stats): array
    {
        return $stats->process();
    }
}
