<?php

namespace App\Controller;

use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\ArticleMetadataGuesser\ArticleMetadataGuesser;
use App\ArticleMetadataGuesser\GuesserContext;

class ArticlesController extends FOSRestController
{
    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    /**
     * @Rest\Get("")
     *
     * @Rest\View
     *
     * @return Article[]
     */
    public function getArticlesAction(): array
    {
        return $this->articleRepository->findAll();
    }

    /**
     * @Rest\Get("/export")
     *
     * @Rest\View(serializerGroups={"Export"})
     *
     * @return Article[]
     */
    public function getArticlesExportAction(): array
    {
        return $this->articleRepository->findEnabledArticles();
    }

    /**
     * @Rest\Get("/stats")
     *
     * @Rest\View
     *
     * @return int[]
     */
    public function getArticlesStatsAction(): array
    {
        return $this->articleRepository->countRealAndFakeArticles();
    }

    /**
     * @Rest\Get("/{id}", requirements={"id"="\d+"})
     *
     * @Rest\View
     *
     * @return Article
     */
    public function getArticleAction($id): Article
    {
        $article = $this->articleRepository->findOneById($id);

        if (null === $article) {
            throw $this->createNotFoundException("Article $id not found");
        }

        return $article;
    }

    /**
     * Returns multiple articles, including one fake.
     *
     * @Rest\Get("/which-one-is-fake")
     *
     * @Rest\QueryParam(name="exceptIds", nullable=true)
     *
     * @Rest\View
     *
     * @return Article[]
     */
    public function getWhichOneIsFakeAction(ParamFetcherInterface $fetcher): array
    {
        $exceptIds = explode(',', $fetcher->get('exceptIds'));

        $articles = array_merge(
            $this->articleRepository->findRandomRealArticles(2, $exceptIds),
            $this->articleRepository->findRandomFakeArticles(1, $exceptIds)
        );

        \shuffle($articles);

        return $articles;
    }

    /**
     * Returns list of articles from list of id.
     *
     * @Rest\Get("/which-one-is-fake/{ids}")
     *
     * @Rest\View
     *
     * @return Article[]|View
     *
     * @throws NotFoundHttpException When an id form ids does not match an article id.
     */
    public function getCustomWhichOneIsFakeAction(string $ids)
    {
        $ids = explode(',', $ids);
        $articles = $this->articleRepository->findArticlesByIds($ids);
        $indexedArticles = [];

        if (count($articles) !== count($ids)) {
            throw $this->createNotFoundException(
                'One of these articles does not exists, or ids malformed (expected "x,x,x")'
            );
        }

        $fakeArticles = array_filter($articles, function (Article $article) {
            return $article->getFake();
        });

        if (1 !== count($fakeArticles)) {
            return View::create(
                'Exactly one article must be fake, got '.count($fakeArticles),
                Response::HTTP_BAD_REQUEST
            );
        }

        foreach ($articles as $article) {
            $indexedArticles[$article->getId()] = $article;
        }

        return array_map(function ($id) use ($indexedArticles) {
            return $indexedArticles[$id];
        }, $ids);
    }

    /**
     * Parses an url to article,
     * and returns a guessed article instance.
     *
     * @Rest\Get("/from-url")
     *
     * @Rest\QueryParam(name="url", strict=true)
     *
     * @Rest\View
     *
     * @return Article
     */
    public function getArticleFromUrl(ParamFetcherInterface $fetcher, ArticleMetadataGuesser $guesser): Article
    {
        $url = $fetcher->get('url');

        return $guesser->guess(new GuesserContext($url));
    }

    /**
     * @Rest\Post("")
     *
     * @ParamConverter("article", converter="fos_rest.request_body")
     *
     * @Rest\View
     *
     * @return View|void
     */
    public function postArticleAction(Article $article, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            return View::create($validationErrors, Response::HTTP_CONFLICT);
        }

        if (Article::NEED_MODERATION) {
            $article->setEnabled(false);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($article);
        $em->flush();
    }
}
