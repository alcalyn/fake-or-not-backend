<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 *
 * @UniqueEntity("url")
 * @UniqueEntity("title")
 */
class Article
{
    /**
     * Whether submitted articles can already be used in game,
     * or need to be moderated first.
     *
     * @var bool
     */
    public const NEED_MODERATION = true;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @JMS\Groups({"Default"})
     */
    private $id;

    /**
     * @Assert\NotNull()
     * @Assert\Url()
     *
     * @JMS\Groups({"Default", "Export"})
     *
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $url;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     *
     * @JMS\Groups({"Default", "Export"})
     *
     * @ORM\Column(type="string", length=511, unique=true)
     */
    private $title;

    /**
     * @Assert\NotNull()
     *
     * @JMS\Groups({"Default", "Export"})
     *
     * @ORM\Column(type="boolean")
     */
    private $fake;

    /**
     * @Assert\NotNull()
     *
     * @JMS\Type("DateTime<'Y-m-d'>")
     * @JMS\Groups({"Default", "Export"})
     *
     * @ORM\Column(type="datetime")
     */
    private $datePublished;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled = true;

    /**
     * Used for stats
     *
     * @JMS\Groups({"Stats"})
     *
     * @var int
     */
    public $answered;

    /**
     * Used for stats
     *
     * @JMS\Groups({"Stats"})
     *
     * @var int
     */
    public $notAnswered;

    /**
     * Used for stats
     *
     * @JMS\Groups({"Stats"})
     *
     * @var float
     */
    public $total;

    /**
     * Used for stats
     *
     * @JMS\Groups({"Stats"})
     *
     * @var float
     */
    public $ratio;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getFake(): ?bool
    {
        return $this->fake;
    }

    public function setFake(bool $fake): self
    {
        $this->fake = $fake;

        return $this;
    }

    public function getDatePublished(): ?\DateTimeInterface
    {
        return $this->datePublished;
    }

    public function setDatePublished(\DateTimeInterface $datePublished): self
    {
        $this->datePublished = $datePublished;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }
}
