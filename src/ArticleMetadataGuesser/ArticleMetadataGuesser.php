<?php

namespace App\ArticleMetadataGuesser;

use App\Entity\Article;

class ArticleMetadataGuesser
{
    /**
     * @var GuesserInterface[]
     */
    private $guessers;

    public function __construct(iterable $guessers)
    {
        $this->guessers = $guessers;
    }

    public function guess(GuesserContext $context): Article
    {
        $article = new Article();

        $article->setUrl($context->getUrl());

        $guessersToUse = [];

        foreach ($this->guessers as $guesser) {
            if ($guesser->shouldGuess($context, $article)) {
                $guessersToUse []= $guesser;
            }
        }

        usort($guessersToUse, function (GuesserInterface $guesser0, GuesserInterface $guesser1) {
            return $guesser0->getOrder() > $guesser1->getOrder() ? 1 : -1;
        });

        foreach ($guessersToUse as $guesser) {
            $guesser->guess($context, $article);
        }

        return $article;
    }
}
