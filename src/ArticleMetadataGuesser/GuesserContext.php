<?php

namespace App\ArticleMetadataGuesser;

use DOMDocument;
use DOMXPath;

class GuesserContext
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var \DOMDocument
     */
    private $domDocument;

    /**
     * @var \DOMXPath
     */
    private $domXPath;

    public function __construct(string $url)
    {
        $this->url = $url;

        $this->init();
    }

    private function init(): void
    {
        $this->domDocument = new DOMDocument();
        libxml_use_internal_errors(true);
        $html = file_get_contents($this->url);
        $this->domDocument->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
        libxml_clear_errors();

        $this->domXPath = new DOMXPath($this->domDocument);
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getDomDocument(): DOMDocument
    {
        return $this->domDocument;
    }

    public function getDomXPath(): DOMXPath
    {
        return $this->domXPath;
    }
}
