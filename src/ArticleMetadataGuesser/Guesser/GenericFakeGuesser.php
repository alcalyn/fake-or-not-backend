<?php

namespace App\ArticleMetadataGuesser\Guesser;

use App\ArticleMetadataGuesser\GuesserInterface;
use App\ArticleMetadataGuesser\GuesserContext;
use App\Entity\Article;

class GenericFakeGuesser implements GuesserInterface
{
    public function guess(GuesserContext $context, Article $article): void
    {
        $article->setFake(preg_match('/legorafi.fr/', $context->getUrl()));
    }

    public function shouldGuess(GuesserContext $context, Article $article): bool
    {
        return null === $article->getFake();
    }

    public function getOrder(): int
    {
        return -10;
    }
}
