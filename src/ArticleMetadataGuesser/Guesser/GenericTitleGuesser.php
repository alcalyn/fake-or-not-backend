<?php

namespace App\ArticleMetadataGuesser\Guesser;

use Symfony\Component\CssSelector\CssSelectorConverter;
use App\ArticleMetadataGuesser\GuesserInterface;
use App\ArticleMetadataGuesser\GuesserContext;
use App\Entity\Article;

class GenericTitleGuesser implements GuesserInterface
{
    /**
     * @var CssSelectorConverter
     */
    private $converter;

    public function __construct(CssSelectorConverter $converter)
    {
        $this->converter = $converter;
    }

    public function guess(GuesserContext $context, Article $article): void
    {
        // Get from meta
        foreach ([
            'meta[property="og:title"]',
            'meta[property="dc:title"]',
            'meta[name="twitter:title"]',
        ] as $metaSelector) {
            $metaTag = $context->getDomXPath()->query($this->converter->toXPath($metaSelector));
            if ($metaTag->length > 0) {
                $title = $metaTag->item(0)->getAttribute('content');
                $title = $this->trimAlsoNonBreakableSpaces($title);
                $article->setTitle($title);
                return;
            }
        }

        // Get from <title> (bad because often includes media name)
        $titles = $context->getDomDocument()->getElementsByTagName('title');
        if ($titles->length > 0) {
            $title = $titles->item(0)->textContent;
            $title = $this->trimAlsoNonBreakableSpaces($title);

            $article->setTitle($title);
        }
    }

    private function trimAlsoNonBreakableSpaces(string $string): string
    {
        return trim(trim($string), chr(0xC2).chr(0xA0));
    }

    public function shouldGuess(GuesserContext $context, Article $article): bool
    {
        return null === $article->getTitle();
    }

    public function getOrder(): int
    {
        return -10;
    }
}
