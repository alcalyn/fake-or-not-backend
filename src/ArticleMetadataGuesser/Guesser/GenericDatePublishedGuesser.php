<?php

namespace App\ArticleMetadataGuesser\Guesser;

use Symfony\Component\CssSelector\CssSelectorConverter;
use App\ArticleMetadataGuesser\GuesserInterface;
use App\ArticleMetadataGuesser\GuesserContext;
use App\Entity\Article;

class GenericDatePublishedGuesser implements GuesserInterface
{
    /**
     * @var CssSelectorConverter
     */
    private $converter;

    public function __construct(CssSelectorConverter $converter)
    {
        $this->converter = $converter;
    }

    public function guess(GuesserContext $context, Article $article): void
    {
        foreach ([
            'og:article:published_time',
            'article:published_time',
        ] as $metaProperty) {
            $metaTag = $context->getDomXPath()->query($this->converter->toXPath('meta[property="'.$metaProperty.'"]'));
            if ($metaTag->length > 0) {
                $content = $metaTag->item(0)->getAttribute('content');
                $article->setDatePublished(new \DateTime($content));
                return;
            }
        }

        // Get date from url
        $matches = [];
        if (preg_match('#([0-9]{4})/([0-9]{2})/([0-9]{2})#', $context->getUrl(), $matches)) {
            array_shift($matches);
            $article->setDatePublished(new \DateTime(implode('-', $matches)));
            return;
        }
    }

    public function shouldGuess(GuesserContext $context, Article $article): bool
    {
        return null === $article->getDatePublished();
    }

    public function getOrder(): int
    {
        return -10;
    }
}
