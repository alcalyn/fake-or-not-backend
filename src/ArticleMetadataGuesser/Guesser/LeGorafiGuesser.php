<?php

namespace App\ArticleMetadataGuesser\Guesser;

use App\ArticleMetadataGuesser\GuesserInterface;
use App\ArticleMetadataGuesser\GuesserContext;
use App\Entity\Article;

class LeGorafiGuesser implements GuesserInterface
{
    public function guess(GuesserContext $context, Article $article): void
    {
        $article->setFake(true);

        // remove " — Le Gorafi.fr Gorafi News Network" suffix
        $titleWithoutSuffix = preg_replace('/—.*Gorafi.*$/', '', $article->getTitle());
        $article->setTitle(trim($titleWithoutSuffix));
    }

    public function shouldGuess(GuesserContext $context, Article $article): bool
    {
        return preg_match('/legorafi.fr/', $context->getUrl());
    }

    public function getOrder(): int
    {
        return 0;
    }
}
