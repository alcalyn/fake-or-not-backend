<?php

namespace App\ArticleMetadataGuesser;

use App\Entity\Article;

interface GuesserInterface
{
    public function guess(GuesserContext $context, Article $article): void;

    public function shouldGuess(GuesserContext $context, Article $article): bool;

    public function getOrder(): int;
}
