<?php

namespace App\Service;

use App\Entity\Article;
use App\Entity\AnswerStat;
use App\Repository\ArticleRepository;
use App\Repository\AnswerStatRepository;

class Stats
{
    /**
     * @var int
     */
    public const MINIMUM_ANSWERS_REQUIRED = 3;

    /**
     * @var int
     */
    public const LIMIT_ARTICLES_RETURNED_IN_TOP = 10;

    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    /**
     * @var AnswerStatRepository
     */
    private $answerStatRepository;

    public function __construct(
        ArticleRepository $articleRepository,
        AnswerStatRepository $answerStatRepository
    ) {
        $this->articleRepository = $articleRepository;
        $this->answerStatRepository = $answerStatRepository;
    }

    /**
     * Process some stats to get top articles in certains criterias.
     *
     * @return Article[][]
     */
    public function process(): array
    {
        /** @var Article[] $articles */
        $articles = $this->articleRepository
            ->findEnabledArticles()
        ;

        /** @var AnswerStat[] $answerStats */
        $answerStats = $this->answerStatRepository
            ->findAll()
        ;

        /** @var Article[] $articles */
        $indexedArticles = [];

        // Index articles by ids and init stats
        foreach ($articles as $article) {
            $indexedArticles[$article->getId()] = $article;
            $article->answered = 0;
            $article->notAnswered = 0;
        }

        // Process sums of answered / not answered
        foreach ($answerStats as $answerStat) {
            foreach ($answerStat->getArticleIds() as $id) {
                if ($answerStat->getAnswer() === $id) {
                    $indexedArticles[$id]->answered++;
                } else {
                    $indexedArticles[$id]->notAnswered++;
                }
            }
        }

        // Process totals and answered ratio
        foreach ($articles as $article) {
            $article->total = $article->answered + $article->notAnswered;
            $article->ratio = 0;

            if ($article->total > 0) {
                $article->ratio = $article->answered / $article->total;
            }
        }

        // Get best articles

        /** @var Article[] $articles */
        $articleWithFewStats = array_filter($articles, function (Article $article) {
            return $article->total >= self::MINIMUM_ANSWERS_REQUIRED;
        });

        $fakeThatSeemsReals = array_filter($articleWithFewStats, function (Article $article) {
            return $article->getFake();
        });
        usort($fakeThatSeemsReals, function (Article $a, Article $b) {
            return $a->ratio > $b->ratio ? 1 : -1;
        });

        $realThatSeemsFakes = array_filter($articleWithFewStats, function (Article $article) {
            return !$article->getFake();
        });
        usort($realThatSeemsFakes, function (Article $a, Article $b) {
            return $a->ratio < $b->ratio ? 1 : -1;
        });

        return [
            'statsGeneratedAt' => new \DateTime('now'),
            'fakeThatSeemsReals' => array_slice($fakeThatSeemsReals, 0, self::LIMIT_ARTICLES_RETURNED_IN_TOP),
            'realThatSeemsFakes' => array_slice($realThatSeemsFakes, 0, self::LIMIT_ARTICLES_RETURNED_IN_TOP),
        ];
    }
}
