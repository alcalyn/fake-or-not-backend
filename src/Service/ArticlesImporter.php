<?php

namespace App\Service;

use JMS\Serializer\SerializerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\Article;

class ArticlesImporter
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator, SerializerInterface $serializer)
    {
        $this->em = $em;
        $this->validator = $validator;
        $this->serializer = $serializer;
    }

    public function import(string $filename): array
    {
        $json = file_get_contents($filename);

        $results = [
            'imported' => 0,
            'failed' => 0,
            'errors' => [],
        ];

        $articles = $this->serializer->deserialize($json, 'array<'.(Article::class).'>', 'json');

        foreach ($articles as $article) {
            if (count($errors = $this->validator->validate($article)) > 0) {
                $results['failed']++;
                $results['errors'] []= [
                    'article' => $article,
                    'errors' => $errors,
                ];

                continue;
            }

            $this->em->persist($article);
            $results['imported']++;
        }

        $this->em->flush();

        return $results;
    }
}
